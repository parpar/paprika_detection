import os
import sys
import random
import math
import re
import time
import numpy as np
import tensorflow as tf
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.patches as patches

# Root directory of the project
ROOT_DIR = os.path.abspath("../../")

# Import Mask RCNN
sys.path.append(ROOT_DIR)  # To find local version of the library
from mrcnn import utils
from mrcnn import visualize
from mrcnn.visualize import display_images
import mrcnn.model as modellib
from mrcnn.model import log

from samples.paprika import paprika

# Directory to save logs and trained model
MODEL_DIR = os.path.join(ROOT_DIR, "logs")

# Path to Paprika trained weights
# You can download this file from the Releases page
# https://github.com/matterport/Mask_RCNN/releases
PAPRIKA_WEIGHTS_PATH = os.path.join(ROOT_DIR, "logs/mask_rcnn_paprika.h5")  # TODO: update this path


###################################################
# Configurations
###################################################
config = paprika.PaprikaConfig()
PAPRIKA_DIR = os.path.join(ROOT_DIR, "datasets/paprika")

# Override the training configurations with a few
# changes for inferencing.
class InferenceConfig(config.__class__):
    # Run detection on one image at a time
    GPU_COUNT = 1
    IMAGES_PER_GPU = 1

config = InferenceConfig()
config.display()


###################################################
# Notebook Preferences
###################################################

# Device to load the neural network on.
# Useful if you're training a model on the same
# machine, in which case use CPU and leave the
# GPU for training.
DEVICE = "/gpu:0"  # /cpu:0 or /gpu:0

# Inspect the model in training or inference modes
# values: 'inference' or 'training'
# TODO: code for 'training' test mode not ready yet
TEST_MODE = "inference"

def get_ax(rows=1, cols=1, size=16):
    """Return a Matplotlib Axes array to be used in
    all visualizations in the notebook. Provide a
    central point to control graph sizes.

    Adjust the size attribute to control how big to render images
    """
    _, ax = plt.subplots(rows, cols, figsize=(size * cols, size * rows))
    return ax


###################################################
# Load Validation Dataset
###################################################

# Load validation dataset
dataset = paprika.PaprikaDataset()
dataset.load_paprika(PAPRIKA_DIR, "val")

# Must call before using the dataset
dataset.prepare()

print("Images: {}\nClasses: {}".format(len(dataset.image_ids), dataset.class_names))


###################################################
# Load Model
###################################################


# Create model in inference mode
with tf.device(DEVICE):
    model = modellib.MaskRCNN(mode="inference", model_dir=MODEL_DIR,
                              config=config)
# Set path to paprika weights file

# Download file from the Releases page and set its path
# https://github.com/matterport/Mask_RCNN/releases
# weights_path = "/path/to/mask_rcnn_paprika.h5"

# Or, load the last model you trained
weights_path = os.path.join(MODEL_DIR, "mask_rcnn_paprika.h5")

# Load weights
print("Loading weights ", weights_path)
model.load_weights(weights_path, by_name=True)


###################################################
# Run Detection
###################################################

# image_id = random.choice(dataset.image_ids)

#image_ids = np.random.choice(dataset.image_ids, 15)
for image_id in dataset.image_ids:

    image, image_meta, gt_class_id, gt_bbox, gt_mask =\
        modellib.load_image_gt(dataset, config, image_id, use_mini_mask=False)
    info = dataset.image_info[image_id]
    print("image ID: {}.{} ({}) {}".format(info["source"], info["id"], image_id,
                                           dataset.image_reference(image_id)))

    # Run object detection
    results = model.detect([image], verbose=1)

    # Display results

    ax = get_ax(1)
    r = results[0]
    visualize.display_instances(image, r['rois'], r['masks'], r['class_ids'],
                                dataset.class_names, r['scores'], ax=ax,
                                title="Predictions")
    log("gt_class_id", gt_class_id)
    log("gt_bbox", gt_bbox)
    log("gt_mask", gt_mask)

# splash = paprika.color_splash(image, r['masks'])
# display_images([splash], cols=1)
#
# ###################################################
# # Visualize Activations
# ###################################################
#
# # Get activations of a few sample layers
# activations = model.run_graph([image], [
#     ("input_image",        tf.identity(model.keras_model.get_layer("input_image").output)),
#     ("res2c_out",          model.keras_model.get_layer("res2c_out").output),
#     ("res3c_out",          model.keras_model.get_layer("res3c_out").output),
#     ("res4w_out",          model.keras_model.get_layer("res4w_out").output),  # for resnet100
#     ("rpn_bbox",           model.keras_model.get_layer("rpn_bbox").output),
#     ("roi",                model.keras_model.get_layer("ROI").output),
# ])
#
# # Input image (normalized)
# _ = plt.imshow(modellib.unmold_image(activations["input_image"][0],config))
#
# # Backbone feature map
# display_images(np.transpose(activations["res2c_out"][0,:,:,:4], [2, 0, 1]), cols=4)

    plt.show()
