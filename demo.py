import os
import sys
import random
import math
import numpy as np
import skimage.io
import matplotlib
import matplotlib.pyplot as plt

# Root directory of the project
ROOT_DIR = os.path.abspath("")

# Import Mask RCNN
sys.path.append(ROOT_DIR)  # To find local version of the library
from mrcnn import utils
import mrcnn.model as modellib
from mrcnn import visualize
# Import PAPRIKA config

#sys.path.append(os.path.join(ROOT_DIR, "samples/paprika/"))  # To find local version

#import paprika
from samples.paprika import paprika


# Directory to save logs and trained model
MODEL_DIR = os.path.join(ROOT_DIR, "logs")

# Local path to trained weights file
PAPRIKA_MODEL_PATH = os.path.join(MODEL_DIR, "mask_rcnn_paprika.h5")


# Directory of images to run detection on
IMAGE_DIR = os.path.join(ROOT_DIR, "images")

class InferenceConfig(paprika.PaprikaConfig().__class__):
    # Set batch size to 1 since we'll be running inference on
    # one image at a time. Batch size = GPU_COUNT * IMAGES_PER_GPU
    GPU_COUNT = 1
    IMAGES_PER_GPU = 1

config = InferenceConfig()
config.display()


############################################################
# Create Model and Load Trained Weights
############################################################
# Create model object in inference mode.
model = modellib.MaskRCNN(mode="inference", model_dir=MODEL_DIR, config=config)

# Load weights trained
model.load_weights(PAPRIKA_MODEL_PATH, by_name=True)


############################################################
# Class Names
############################################################

# PAPRIKA Class names
# Index of the class in the list is its ID. For example, to get ID of
# the teddy bear class, use: class_names.index('teddy bear')
class_names = ['BG', 'paprika']


############################################################
# Run Object Detection
############################################################

# Load images from the images folder
file_names = next(os.walk(IMAGE_DIR))[2]
for file_name in file_names:

    image = skimage.io.imread(os.path.join(IMAGE_DIR, file_name))

    # Run detection
    results = model.detect([image], verbose=1)

    # Visualize results
    r = results[0]
    visualize.display_instances(image, r['rois'], r['masks'], r['class_ids'],
                                class_names, r['scores'])


